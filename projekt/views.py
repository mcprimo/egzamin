from models import obliczenie
from forms import Formularz
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.context_processors import  csrf
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
import datetime
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
import requests
import json
from lxml import objectify
from tasks import operacje
from tasks import oblicz
from tasks import rejestracja
from djcelery import views as celery_views
import simplejson as sjson
import couchdb

def home(request):
    form=Formularz()
    return render_to_response("index.html",
    {'form': form})

def rejestruj(request):
    rejestracja.delay()
    return HttpResponse(" ")

def wykonaj_in(request):
    if request.method == 'POST': # If the form has been submitted...
        form = Formularz(request.POST) # A form bound to the POST data
        if form.is_valid():# All validation rules pass
            pole=form.cleaned_data["pole"]
            id=oblicz.delay(pole)
            return HttpResponse(id.id)

def wykonaj(request,id=1):
    result = oblicz.AsyncResult(id)
    if not result.ready():
        return HttpResponse('wait')
    else:
        res=result.get(timeout=10)
        return HttpResponse(res)

def licz(request):
    odp={"success":"false", "result":"null"}
    if request.method == 'POST': # If the form has been submitted...
        try:
            data = sjson.loads(request.body)
            wyn=float(data['number1'])+float(data['number2'])
            id=data['id']
            odp={"id":id,"success":"true", "result":wyn}
        except:
            odp={"success":"false", "result":"null"}
    return HttpResponse(json.dumps(odp))


def dostepne_in(request):
    id=operacje.delay("+")
    return HttpResponse(id.id)

def dostepne(request, id=1):
    result = operacje.AsyncResult(id)
    if not result.ready():
        return HttpResponse('wait')
    else:
        res=result.get(timeout=10)
        return HttpResponse(res)

def js(request):
    return render_to_response("jquery-2.0.2.min.js")