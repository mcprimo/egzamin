import os, sys

sys.path.append('C:\www')
sys.path.append('C:\www\projekt')
sys.path.append('C:\www\projekt\projekt')
sys.path.append('C:\Python26\Lib\site-packages\django\contrib\admin\static')
os.environ['DJANGO_SETTINGS_MODULE'] = 'projekt.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()